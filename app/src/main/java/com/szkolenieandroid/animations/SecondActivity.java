package com.szkolenieandroid.animations;

import android.app.Activity;
import android.os.Bundle;


public class SecondActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_second);
    }


    @Override
    protected void onResume() {
        super.onResume();
        overridePendingTransition(R.anim.pull_in_from_right, R.anim.hold);
    }

    @Override
    protected void onPause() {
        super.onPause();
        overridePendingTransition(R.anim.hold, R.anim.push_out_to_right);
    }
}
