package com.szkolenieandroid.animations;

import android.animation.AnimatorSet;
import android.animation.ArgbEvaluator;
import android.animation.ObjectAnimator;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.animation.BounceInterpolator;
import android.widget.RelativeLayout;
import android.widget.TextView;


public class AnimationsActivity extends Activity {

    TextView textView;
    RelativeLayout rootContainer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_animations);
        textView = (TextView) findViewById(R.id.textView);
        rootContainer = (RelativeLayout) findViewById(R.id.rootContainer);
    }


    public void runAnimation(View view) {
        startActivity(new Intent(this, SecondActivity.class));
    }

    private void animateFluentWithRemove(final TextView textView) {
        textView.animate().alpha(0).setDuration(1000).withEndAction(new Runnable() {
            public void run() {
                rootContainer.removeView(textView);
            }
        });
    }
    private void animateFluent(final TextView textView) {
        textView.animate().alpha(0).x(50f).y(200f).setDuration(1000).withEndAction(new Runnable() {
            public void run() {
                rootContainer.removeView(textView);
            }
        });
    }

    private void rotate3d(View animatedView) {
        int dest = 360;
        if(animatedView.getRotationX() == dest) {
            dest = 0;
        }
        ObjectAnimator animator = ObjectAnimator
                .ofFloat(animatedView, "rotationX", dest);

        ObjectAnimator animatorY = ObjectAnimator
                .ofFloat(animatedView, "rotationY", dest);
        animator.setDuration(2000);
        animatorY.setDuration(2000);
        AnimatorSet set = new AnimatorSet();
        set.playTogether(animator, animatorY);
        set.start();

    }

    private void rotate(TextView animatedView) {
        int dest = 360;
        if(animatedView.getRotation() == dest) {
            dest = 0;
        }
        ObjectAnimator animator = ObjectAnimator
                .ofFloat(animatedView, "rotation", dest);

        animator.setDuration(2000);
        animator.start();

    }

    private void animateColor(TextView textView) {
        Integer colorFrom = getResources().getColor(android.R.color.holo_red_light);
        Integer colorTo = getResources().getColor(android.R.color.holo_green_dark);
        ObjectAnimator animator = ObjectAnimator
                .ofObject(textView, "textColor",
                        new ArgbEvaluator(),colorFrom,colorTo);
        animator.setDuration(2000);
        animator.start();
    }

    private void moveXY(View animatedView) {

        int dest = 350;
        if(animatedView.getX() == dest) {
            dest = 0;
        }
        ObjectAnimator animator = ObjectAnimator
                .ofFloat(animatedView, "x", dest);

        ObjectAnimator animatorY = ObjectAnimator
                .ofFloat(animatedView, "y", dest);
        animator.setDuration(2000);
        animatorY.setDuration(2000);

        animator.setInterpolator(new BounceInterpolator());
        animatorY.setInterpolator(new BounceInterpolator());

        AnimatorSet set = new AnimatorSet();
        set.playTogether(animator, animatorY);
        set.start();

//        animator.start();

    }
}
